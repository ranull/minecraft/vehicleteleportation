package com.ranull.vehicleteleportation;

import com.ranull.vehicleteleportation.command.VehicleTeleportationCommand;
import com.ranull.vehicleteleportation.integration.WorldGuard;
import com.ranull.vehicleteleportation.listener.PlayerTeleportListener;
import com.ranull.vehicleteleportation.manager.VehicleManager;
import org.bstats.bukkit.MetricsLite;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public final class VehicleTeleportation extends JavaPlugin {
    private WorldGuard worldGuard;
    private VehicleManager vehicleManager;

    @Override
    public void onLoad() {
        if (getServer().getPluginManager().getPlugin("WorldGuard") != null) {
            try {
                Class.forName("com.sk89q.worldguard.WorldGuard", false, getClass().getClassLoader());
                Class.forName("com.sk89q.worldguard.protection.flags.registry.FlagConflictException",
                        false, getClass().getClassLoader());

                worldGuard = new WorldGuard(this);
            } catch (ClassNotFoundException ignored) {
            }
        }
    }

    @Override
    public void onEnable() {
        vehicleManager = new VehicleManager(this);

        saveDefaultConfig();
        registerMetrics();
        registerCommands();
        registerListeners();
    }

    @Override
    public void onDisable() {
        unregisterListeners();
    }

    private void registerMetrics() {
        new MetricsLite(this, 17373);
    }


    private void registerCommands() {
        PluginCommand vehicleTeleportationPluginCommand = getCommand("vehicleteleportation");

        if (vehicleTeleportationPluginCommand != null) {
            VehicleTeleportationCommand vehicleTeleportationCommand = new VehicleTeleportationCommand(this);

            vehicleTeleportationPluginCommand.setExecutor(vehicleTeleportationCommand);
            vehicleTeleportationPluginCommand.setTabCompleter(vehicleTeleportationCommand);
        }
    }


    public void registerListeners() {
        getServer().getPluginManager().registerEvents(new PlayerTeleportListener(this), this);
    }

    public void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }

    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }

    public WorldGuard getWorldGuard() {
        return worldGuard;
    }
}
