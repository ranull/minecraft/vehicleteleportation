package com.ranull.vehicleteleportation.command;

import com.ranull.vehicleteleportation.VehicleTeleportation;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VehicleTeleportationCommand implements CommandExecutor, TabExecutor {
    private final VehicleTeleportation plugin;

    public VehicleTeleportationCommand(VehicleTeleportation plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command,
                             @NotNull String string, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(ChatColor.YELLOW + "VehicleTeleportation " + ChatColor.DARK_GRAY
                    + ChatColor.RESET + ChatColor.DARK_GRAY + "v" + plugin.getDescription().getVersion());

            commandSender.sendMessage(ChatColor.YELLOW + "/vehicleteleportation " + ChatColor.DARK_GRAY + "-"
                    + ChatColor.RESET + " Plugin info");

            if (commandSender.hasPermission("vehicleteleportation.reload")) {
                commandSender.sendMessage(ChatColor.YELLOW + "/vehicleteleportation reload " + ChatColor.DARK_GRAY + "-"
                        + ChatColor.RESET + " Reload plugin");
            }

            commandSender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.YELLOW + "Ranull");
        } else if (args[0].equals("reload")) {
            if (commandSender.hasPermission("vehicleteleportation.reload")) {
                plugin.saveDefaultConfig();
                plugin.reloadConfig();
                commandSender.sendMessage(ChatColor.YELLOW + "VehicleTeleportation" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "Reloaded config file.");
            } else {
                commandSender.sendMessage(ChatColor.YELLOW + "VehicleTeleportation" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "No permission.");
            }
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command,
                                      @NotNull String string, @NotNull String @NotNull [] args) {
        if (commandSender.hasPermission("vehicleteleportation.reload")) {
            return Collections.singletonList("reload");
        }

        return new ArrayList<>();
    }
}
