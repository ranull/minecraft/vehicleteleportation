package com.ranull.vehicleteleportation.listener;

import com.ranull.vehicleteleportation.VehicleTeleportation;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

public class PlayerTeleportListener implements Listener {
    private final VehicleTeleportation plugin;

    public PlayerTeleportListener(VehicleTeleportation plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerTeleportEvent(PlayerTeleportEvent event) {
        if (event.getPlayer().getVehicle() != null
                && !event.getCause().toString().equals("UNKNOWN")
                && !event.getCause().toString().equals("DISMOUNT")
                && event.getFrom().getWorld() != null
                && event.getTo() != null && event.getTo().getWorld() != null
                && plugin.getVehicleManager().isWorldValid(event.getFrom().getWorld())
                && plugin.getVehicleManager().isWorldValid(event.getTo().getWorld())
                && event.getPlayer().hasPermission("vehicleteleportation.use")
                && (plugin.getWorldGuard() == null || (plugin.getWorldGuard().shouldTeleport(event.getTo())
                && plugin.getWorldGuard().shouldTeleport(event.getFrom())))) {
            plugin.getVehicleManager().teleportVehicle(event.getPlayer(), event.getTo(), event.getCause());
        }
    }
}
