package com.ranull.vehicleteleportation.manager;

import com.ranull.vehicleteleportation.VehicleTeleportation;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VehicleManager {
    private final VehicleTeleportation plugin;

    public VehicleManager(VehicleTeleportation plugin) {
        this.plugin = plugin;
    }

    public void teleportVehicle(Entity entity, Location location, PlayerTeleportEvent.TeleportCause teleportCause) {
        if (entity.getVehicle() != null) {
            Entity vehicleEntity = entity.getVehicle();
            List<Entity> passengerList = getPassengers(vehicleEntity);
            Chunk chunk = vehicleEntity.getLocation().getChunk();

            plugin.getVehicleManager().addPluginChunkTicket(chunk);

            plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
                vehicleEntity.eject();
                vehicleEntity.teleport(location, teleportCause);

                for (Entity passengerEntity : passengerList) {
                    if (entity.getUniqueId().equals(passengerEntity.getUniqueId())) {
                        addPassenger(vehicleEntity, passengerEntity);
                    } else {
                        plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
                            passengerEntity.teleport(location, teleportCause);
                            addPassenger(vehicleEntity, passengerEntity);
                        }, 1L);
                    }
                }

                removePluginChunkTicket(chunk);
            }, 3L);
        }
    }

    public void addPluginChunkTicket(Chunk chunk) {
        try {
            chunk.addPluginChunkTicket(plugin);
        } catch (NoSuchMethodError ignored) {
        }
    }

    public void removePluginChunkTicket(Chunk chunk) {
        try {
            chunk.removePluginChunkTicket(plugin);
        } catch (NoSuchMethodError ignored) {
        }
    }

    public boolean isWorldValid(World world) {
        List<String> stringList = plugin.getConfig().getStringList("worlds");

        return stringList.contains("ALL") || stringList.contains(world.getName());
    }

    @SuppressWarnings("deprecation")
    private List<Entity> getPassengers(Entity entity) {
        try {
            return new ArrayList<>(entity.getPassengers());
        } catch (NoSuchMethodError ignored) {
            return entity.getPassenger() != null ? Collections.singletonList(entity.getPassenger()) : new ArrayList<>();
        }
    }

    @SuppressWarnings("deprecation")
    private void addPassenger(Entity entity, Entity passengerEntity) {
        try {
            entity.addPassenger(passengerEntity);
        } catch (NoSuchMethodError ignored) {
            entity.setPassenger(passengerEntity);
        }
    }
}
